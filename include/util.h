#ifndef CHIP8_GUI_UTIL_H
#define CHIP8_GUI_UTIL_H

#include <stdbool.h>

void util_rand_init();
int util_rand();

bool is_valid_key_code(int key);

void set_pixel(int x, int y);
bool is_pixel_set(int x, int y);

void bcd(int dec);
int key_to_map(int key);
int map_to_key(int map);

void print_debug_info();
void print_debug_and_exit();
	
void hexDump (char *desc, void *addr, int len);
#endif
