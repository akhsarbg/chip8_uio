CC=gcc
SRC=src
INCLUDE=include
CFLAGS=-lm -lncurses -g -I$(INCLUDE)
OUTPUT=chip8
SRCS = \
	$(SRC)/main.c 	 	\
	$(SRC)/chip8.c 	 	\
	$(SRC)/cpu.c 	 	\
	$(SRC)/io.c 	 	\
	$(SRC)/util.c


chip8: $(SRCS)
	$(CC) -Wall -Wextra -o $(OUTPUT) $(SRCS) $(CFLAGS)

.PHONY: clean

clean:
	rm -f $(OUTPUT)
