#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <stdbool.h>
#include <string.h>
#include <unistd.h>
#include <ncurses.h>
#include "chip8.h"
#include "cpu.h"
#include "io.h"
#include "util.h"

int mx, my;
WINDOW* win;

int io_video_init(){
  initscr();

  noecho();
  cbreak();
  curs_set(FALSE);
  start_color();
  
  getmaxyx(stdscr, my, mx);
  printf("Max: (%d,%d)\n", mx, my);

  win = newwin(VIDEO_HEIGHT, VIDEO_WEIGHT*2,
	       my/2-VIDEO_HEIGHT/2, mx/2-VIDEO_WEIGHT);
  //box(win, '-', '-');
  wrefresh(win);

  init_pair(1, COLOR_WHITE, COLOR_WHITE);
  init_pair(1, COLOR_GREEN, COLOR_GREEN);
  
  //io_video_clear();
  return 0;
}

int io_sound_init(){
  //puts("sound init");
  return 0;
}

int io_keyboard_init(){
  return 0;
}

void io_video_render2(int x, int y, int n){
  int xx, yy;
  char bchar = 'o';

  for(yy=0; yy<n; yy++){
    for(xx=0; xx<BYTE_LENGTH*8; xx++){
      if(is_pixel_set((x+xx)/2, y+yy)){
	wattron(win, COLOR_PAIR(1));
	mvwaddch(win, y+yy,x+xx, bchar);
      }else{
	wattron(win, COLOR_PAIR(2));
	mvwaddch(win, y+yy,x+xx, bchar);
      }
    }
  }

  /* Update the window */
  io_video_reflesh();
}

void io_video_draw(uint8_t x, uint8_t y, uint8_t n){
  int bit, offset = 0;
  uint8_t current;
  /* Update video memory */
  //printf("draw(%d, %d, %d)\n",x,y,n);
  for(offset = 0; offset < n; offset++){
    current = ram_memory[IR + offset];
    //printf("Current: %x\n", current);
    for(bit=0; bit < BYTE_LENGTH; bit++){
      if( ((current >> (BYTE_LENGTH - 1 - bit)) & 0x1) == 1){
	set_pixel((x+bit) % VIDEO_WEIGHT,
		  (y+offset) % VIDEO_HEIGHT);
      }
    }
  }
  /* Draw sprite */
  io_video_render2(x, y, n);
}

void io_video_clear(){
  /* Clear video momory */
  memset(video_memory, 0, VIDEO_LENGTH);
  /* Render empty screen */
  erase();
  io_video_reflesh();
}

void io_video_render(){
  int x, y;
  char bchar = 'o';

  for(y = 0; y < VIDEO_HEIGHT; y++){
    for(x = 0; x < VIDEO_WEIGHT*2; x++){
      if(is_pixel_set(x/2,y)){
	//printf("Pixel %d:%d is set\n", x,y);
	wattron(win, COLOR_PAIR(1));
	mvwaddch(win, y,x, bchar);
      }else{
	wattron(win, COLOR_PAIR(2));
	mvwaddch(win, y,x, bchar);
      }
    }
  }

  /* Update the window */
  io_video_reflesh();
}

void io_video_reflesh(){
  wrefresh(win);
}

bool io_video_running(){
  return true;
}


/* SOUND HANDLE */

void io_sound_play(){
  //puts("audio start");
}

void io_sound_stop(){
  //puts("audio stop");
}

/* KEYBOARD HANDLE */
bool io_key_isPressed(int key){
  key = key_to_map(key);
  nodelay(win, 1);
  return wgetch(win) == key;
}

int io_key_get(){
  nodelay(win, 0);
  char keycode = wgetch(win);
  int key = map_to_key(keycode);
  return key;
}

void io_shutdown(){
  /* Cleanup resources */
  /* Video subsystem shutdown */
  endwin();
}
