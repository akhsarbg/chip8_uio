#include <stdio.h>
#include <stdint.h>
#include <string.h>
#include "io.h"
#include "cpu.h"
#include "chip8.h"
#include "util.h"

uint16_t sptr = (_START - 1) / sizeof(uint16_t);
uint16_t* stack = (uint16_t*)ram_memory;

void stack_init(){
  int i;
  for(i=sptr; i > sptr-11; i--)
    stack[i] = -1;
}

void stack_print(){
  uint16_t i;
  puts("STACK: ");
  for(i = sptr; i < _START/sizeof(uint16_t); i++){
    if(stack[i] != (uint16_t)-1){
      printf("%x\n", (uint16_t)stack[i]);
    }
  }
  puts(";");
}

/* Underflow is not taken care of */
void push(uint16_t data){
  stack[sptr--] = data;
}

/* Overflow is not taken care of */
uint16_t pop(){
  return stack[++sptr];
}

void cpu_tick(){
  uint16_t opcode = -1;
  /* Fetch opcode */
  opcode = ( (ram_memory[PC] << 8) + ram_memory[PC+1]);

  //printf("\e[1;1H\e[2J");
  //printf("Opcode: %X\n\n", opcode);
  //print_debug_info();
  //getchar();

  /* Execute opcode */
  cpu_execute_opcode(opcode);

}

void cpu_execute_opcode(uint16_t op){
  uint8_t op0, op1, op2, op3;

  op0 = ( (op >> 12) & 0xF);
  op1 = ( (op >> 8)  & 0xF);
  op2 = ( (op >> 4)  & 0xF);
  op3 = ( (op >> 0)  & 0xF);

  /* Increment Program Counter */
  PC += sizeof(uint16_t);

  switch(op0){
  case 0:
    switch(op){
    case 0x00E0:
      io_video_clear(); break;
    case 0x00EE:
      PC = pop();
      break;
    default: break;
    }
    break;
  case 1:
    PC = op & 0x0FFF; break;
  case 2:
    push(PC);
    PC = op & 0x0FFF;
    break;
  case 3:
    PC += (V[op1] == (op & 0x00FF) ? 2 : 0); break;
  case 4:
    PC += (V[op1] != (op & 0x00FF) ? 2 : 0); break;
  case 5:
    PC += (V[op1] == V[op2] ? 2 : 0); break;
  case 6:
    V[op1] = op & 0x00FF; break;
  case 7:
    V[op1] += op & 0x00FF; break;
  case 8:
    switch(op3){
    case 0:
      V[op1] = V[op2]; break;
    case 1:
      V[op1] |=  V[op2]; break;
    case 2:
      V[op1] &=  V[op2]; break;
    case 3:
      V[op1] ^=  V[op2]; break;
    case 4:
      V[op1] += V[op2];
      V[0xF] = ( V[op1] + V[op2] > 0xFF ? 1 : 0);
      break;
    case 5:
      V[0xF] = ( V[op1] > V[op2] ? 1 : 0);
      V[op1] -= V[op2];
      break;
    case 6:
      V[0xF] = V[op1] & 0x1;
      V[op1] = V[op1] >> 1;
      break;
    case 7:
      V[0xF] = ( V[op2] > V[op1] ? 1 : 0);
      V[op1] = V[op2] - V[op1];
      break;
    case 0xE:
      V[0xF] = V[op1] >> (BYTE_LENGTH - 1);
      V[op1] = V[op1] << 1;
      break;
    default:
      break;
    }
    break;
  case 9:
    PC += (V[op1] != V[op2] ? 2 : 0); break;
  case 0xA:
    IR = op & 0x0FFF; break;
  case 0xB:
    PC = V[0] + (op & 0x0FFF); break;
  case 0xC:
    V[op1] = util_rand() & (op & 0x00FF); break;
  case 0xD:
    /* Draw sprite*/
    V[0xF] = 0;
    io_video_draw(V[op1], V[op2], op3);
    break;
  case 0xE:
    switch(op & 0x00FF){
    case 0x9E:
      PC += ( io_key_isPressed(V[op1]) ? 2 : 0); break;
    case 0xA1:
      PC += ( !io_key_isPressed(V[op1]) ? 2 : 0); break;
    default: break;
    }
    break;
  case 0xF:
    switch(op & 0x00FF){
    case 0x07:
      V[op1] = delay_timer; break;
    case 0x0A:
      V[op1] = io_key_get(); break;
    case 0x15:
      delay_timer = V[op1]; break;
    case 0x18:
      sound_timer = V[op1]; break;
    case 0x1E:
      IR += V[op1]; break;
    case 0x29:
      IR = V[op1] * char_size;
      break;
    case 0x33:
      bcd(V[op1]);
      break;
    case 0x55:
      memcpy(&ram_memory[IR] , V, (op1+1)*sizeof(uint8_t));
      //IR += op1 + 1;
      break;
    case 0x65:
      memcpy(V, &ram_memory[IR], (op1+1)*sizeof(uint8_t));
      //IR += op1 + 1;
      break;

    default:
      printf("Unknows opcode: %x\n", op);
      print_debug_and_exit();
      break;
    }
    break;
  }
}
