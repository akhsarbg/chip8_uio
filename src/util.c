#include <time.h>
#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <stdbool.h>
#include "util.h"
#include "cpu.h"

char keymap[16][2] = {
  {'1', 0x1},
  {'2', 0x2},
  {'3', 0x3},
  {'4', 0xc},
  {'q', 0x4},
  {'w', 0x5},
  {'e', 0x6},
  {'r', 0xd},
  {'a', 0x7},
  {'s', 0x8},
  {'d', 0x9},
  {'f', 0xe},
  {'z', 0xa},
  {'x', 0x0},
  {'c', 0xb},
  {'v', 0xf},
};

void util_rand_init(){
  srand(time(NULL));   // should only be called once
}

int util_rand(){
  return rand() % 0xFF;;
}

/*
  Used on machines with more keys that chip8

  Fuction is depricated
*/
bool is_valid_key_code(int key){
  int i;
  for(i=0; i<0xF; i++)
    if(key == keymap[i][0])
      return true;
  return false;
}

void set_pixel(int x, int y){
  uint16_t index = (y*VIDEO_WEIGHT + x) / BYTE_LENGTH;
  uint16_t offset = x % BYTE_LENGTH;
  uint16_t bit = (BYTE_LENGTH-1) - offset;

  video_memory[index] ^= ( 1 << bit );

  // No need to perform this is V[0xF]
  // is true from earlier
  if(!V[0xF])
    V[0xF] |=  !(video_memory[index] >> bit & 1);
}

bool is_pixel_set(int x, int y){
  uint8_t b = -1;
  uint16_t index = (y*VIDEO_WEIGHT + x) / BYTE_LENGTH;
  uint16_t offset = x % BYTE_LENGTH;
  uint16_t bit = (BYTE_LENGTH-1) - offset;

  b = (video_memory[index] >> bit) & 1;
  return b;
}

/* Binary-Coded Decimal */
void bcd(int dec){
  int i, sum = 0;

  for(i=0; i<BYTE_LENGTH; i++){
    sum += (dec >> i & 1) * pow(2, i);
  }

  ram_memory[IR+0] = sum % 1000 / 100;
  ram_memory[IR+1] = sum % 100 / 10;
  ram_memory[IR+2] = sum % 10 / 1;
}

int key_to_map(int key){
  int i;
  for(i=0; i<0xF; i++){
    if(key == keymap[i][1]) return keymap[i][0];
  }
  return -1;
}

int map_to_key(int map){
  int i;
  for(i=0; i<0xF; i++){
    if(map == keymap[i][0]) return keymap[i][1];
  }
  return -1;
}

void print_debug_info(){
  int i;
  printf("IR: %X\n", IR);
  printf("PC: %X\n", PC);
  printf("Delay: %X\n", delay_timer);
  printf("Sound: %X\n\n", sound_timer);
  for(i=0; i<0xF; i++){
    printf("V[%x]: %X\n", i, V[i]);
  }
  stack_print();
}

void print_debug_and_exit(){
  print_debug_info();
  exit(1);
}
