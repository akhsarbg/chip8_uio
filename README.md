# CHIP8 emulator #

### CHIP8 Specifications ###

* 16 x 8-bit registers (V0 - VF)
* 1 x 16-bit index register
* 1 x 16-bit program counter
* 1 x 8-bit delay timer
* 1 x 8-bit sound timer
* 64 x 32 bit frame buffer
* 1 x 4096 byte addressable memory  
* 1 x 64-byte stack
  - 1 x 8-bit stack pointer





### Who do I talk to? ###

* [Akhsarbek Gozoev](mailto:akhsarbg@uio.no)